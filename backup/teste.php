<!DOCTYPE html>
<html lang="en">
<head>
    <script>
    // CODELAB: Register service worker.
        if ('serviceWorker' in navigator) {
            window.addEventListener('load', () => {
                navigator.serviceWorker.register('/service-worker.js')
                    .then((reg) => {
                    console.log('Service worker registered.', reg);
                    });
            });
        }
    </script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CODELAB: Add link rel manifest -->
    <link rel="manifest" href="manifest.json">
    <!-- CODELAB: Add description here -->
    <meta name="description" content="A sample weather app">
    <!-- CODELAB: Add meta theme-color -->
   <meta name="theme-color" content="#2F3BA2" />
    
    <!-- CODELAB: Add iOS meta tags and icons -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Weather PWA">
    <link rel="apple-touch-icon" href="/images/icons/icon-152x152.png">

    <title>Document</title>
</head>
<body>
    <?php 
        // Iniciamos a função do CURL:
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, "https://jsonplaceholder.typicode.com/posts");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf-8") );
        $result = curl_exec($ch);

        curl_close($ch);

        $result = json_decode($result, true);

        foreach($result as $item){ 
            echo '<h1>'.$item["title"].'</h1>';
        }
    ?>
    <h1>Hello sasas!</h1>
</body>
</html>